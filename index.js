'use strict'
const AWS = require('aws-sdk')
AWS.config.update({ region: `${process.env.region}` })

function lineNotify(callback){
    console.log(`function lineNotify`);
    //
}

exports.handler = async (event, context, callback) =>{
    console.log(event.requestContext.http.method);
    if(event.requestContext.http.method !== 'POST'){
        context.fail({
            statusCode: 502,
            body: { message: 'wrong method' }
        });
    }
    let pathIsCorrect = true;
    switch(event.requestContext.http.path){
        case "/sns" :
            const sns = new AWS.SNS();
            const params = {
                Message: 'Hello World!',
                TopicArn: `${process.env.TopicArn}`,
            };
            sns.publish(params, (err, data)=>{
                if(err){
                    console.error(`Error`, err);
                    callback(err, null)
                }
                else {
                    console.log(`Success`, data);
                    callback(null, data)
                }
            })
        break;
        case "/line" :
            //
        break;
        default:
            pathIsCorrect = false;
    }

    let response = {
        statusCode: pathIsCorrect ? 203 : 404,
        body: pathIsCorrect 
            ? JSON.stringify({ message: 'successful!' }) 
            : JSON.stringify({ message: 'not found your path' }),
        headers: {
            'Content-Type': 'application/json',
        }
    };
    
    if(pathIsCorrect){
        context.succeed(response);
    }else{
        context.fail(response);
    }
}